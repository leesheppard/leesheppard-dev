# Lee Sheppard ::>Developer<::

	$ npm run develop

will run `$ bundle exec jekyll clean && run-p develop:{webpack,jekyll}`
    
	$ npm run develop:jekyll
	
will run `$ sleep 5 && bundle exec jekyll serve --watch`
    
	$ npm run develop:webpack
	
will run `$ webpack --mode development --watch`
    
	$ npm run build
	
will run `$ run-s build:webpack build:jekyll`
    
	$ npm run build:jekyll
	
will run `$ JEKYLL_ENV=production bundle exec jekyll build`
    
	$ npm run build:webpack
	
will run `$ webpack --mode production`